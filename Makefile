GAS = clang

all: plastic.bin

plastic.o: main.asm
	nasm -f elf32 -o plastic.o main.asm

plastic.bin: plastic.o
	ld -melf_i386 -nostdlib -T link.ld -o plastic.elf plastic.o
	objcopy -O binary plastic.elf plastic.bin

clean:
	rm -f plastic plastic.o plastic.elf plastic.bin

run: all
	qemu-system-x86_64 plastic.bin
