%define BLU 0x1     ; blue
%define GRN 0x2     ; green
%define CYN 0x3     ; cyan
%define RED 0x4     ; red
%define MGT 0x5     ; magenta
%define ORG 0x6     ; orange
%define LITGRY 0x7  ; light grey
%define DRKGRY 0x8  ; dark grey
%define LITBLU 0x10 ; light blue
%define LITGRN 0x11 ; light green
%define LITCYN 0x12 ; light cyan
%define LITRED 0x13 ; light red
%define LITMGT 0x14 ; light magenta
%define YLW 0x15    ; yellow
%define WHT 0x16    ; white

; I think this is pretty much always what you want
%define PAGE 0x01

;; Draw the pixel:
;; AH = 0Ch
;; AL = Color
;; BH = Page Number
;; CX = x
;; DX = y

; %1 : color
; %2 : page
; %3 : x
; %4 : y
%macro drawpx 4
	mov ax, 0x0C88
	mov al, %1
	mov bh, %2
	mov cx, %3
	mov dx, %4
	int 0x10
%endmacro

; draw a whole line
; TODO super broken
; %1 : color
; %2 : x
; %3 : y
; %4 : sub x by
; %5 : sub y by
%macro drawline 5
	; set the initial x and y
	mov cx, %2
	mov dx, %3
	
	; start the loop
	%%start:
		drawpx %1, PAGE, cx, dx
		sub cx, %4
		sub dx, %5

		; check if we should break
		cmp dx, 0
		jle %%end
		cmp cx, 0
		jle %%end
		jmp %%start
	%%end:
%endmacro

; draw a horizontal line
; %1 : color
; %2 : x
; %3 : y
; %4 : length
%macro linex 4
	; set initial x and y
	xor cx, cx
	xor dx, dx
	mov word cx, %2
	mov word dx, %3

	; loop
	%%start:
		drawpx %1, PAGE, cx, dx
		inc cx
		; compare it to the starting point plus the length
		cmp cx, (%4 + %2)
		je %%end
		jmp %%start
	%%end:
%endmacro

; draw a vertical line
; %1 : color
; %2 : x
; %3 : y
; %4 : length
%macro liney 4
	; set initial x and y
	xor cx, cx
	xor dx, dx
	mov cx, %2
	mov dx, %3

	; loop
	%%start:
		drawpx %1, PAGE, cx, dx
		inc dx
		; starting point + length
		cmp dx, (%4 + %2)
		je %%end
		jmp %%start
	%%end:
%endmacro

; draw a square
; %1 : color
; %2 : x
; %3 : y
; %4 : size
%macro square 4
	linex %1, %2, %3, %4
	linex %1, %2, (%3 + %4), %4
	liney %1, %2, %3, %4
	liney %1, (%2 + %4), %3, 0 ; for some reason this only works with len 0
%endmacro

; clear the screen and reset to 13h
%macro clear 0
	mov ah, 0
	int 0x10
	mov ax, 0x0013
	int 0x10
%endmacro

; wait a number of miliseconds
; %1 : time
%macro wait 1
	mov cx, %1
	mov dx, %1
	mov ah, 86H
	int 15H
%endmacro

[BITS 16]
cli

start:
	xor ax, ax
	; must zero ds for data access
	mov ds, ax
	; the rest aren't mandatory
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov bp, ax
	mov ss, ax
	mov sp, ax

	; setup
	; set the video mode to 13h
	mov ax, 0x0013
	int 0x10

	mov ax, 0

loop:
	wait 0x1

	inc ax
	square BLU, 0, 0, 0x40
	square RED, 40, 40, 0x50

	; wait
	wait 0x1

	clear
	jmp loop

end:
	hlt
